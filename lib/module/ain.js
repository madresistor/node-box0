/*
 * This file is part of node-box0.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * node-box0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * node-box0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with node-box0.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

var ref = require("ref")
var _binding = require("../_binding.js")
var act = require("../exception.js").act
var module = require("./module.js")

function build_mode_array(item) {
	var result = []

	var count = item.count
	var values = item.values.reinterpret(count * _binding.b0_ain_bitsize_speeds.size)

	for (var i = 0; i < count; i++) {
		var byte_offset = i * _binding.b0_ain_bitsize_speeds.size
		var bss = ref.get(values, byte_offset, _binding.b0_ain_bitsize_speeds)

		result.push({
			bitsize: bss.bitsize,
			speed: _binding.as_ulong_array(bss.speed.values, bss.speed.count)
		})
	}

	return result
}

var Ain = function(device, index) {
	var pointer = ref.alloc(_binding.ptr_b0_ain)
	act(_binding.b0_ain_open(device._pointer, pointer, index))
	this._pointer = pointer.deref()

	var obj = this._pointer.deref()

	this.header = {
		type: obj.header.type,
		index: obj.header.index,
		name: obj.header.name,
		device: device
	}

	this.chan_count = obj.chan_count
	this.buffer_size = obj.buffer_size
	this.capab = obj.capab

	this.label = {
		chan: _binding.strings_to_array(obj.label.chan, obj.chan_count)
	}

	this.ref = {
		low: obj.ref_.low,
		high: obj.ref_.high,
		type: obj.ref_.type,
	}

	this.stream = build_mode_array(obj.stream)
	this.snapshot = build_mode_array(obj.snapshot)
}

Ain.prototype.bitsize_speed_set = function (bitsize, speed) {
	act(_binding.b0_ain_bitsize_speed_set(this._pointer, bitsize, speed))
}

Ain.prototype.bitsize_speed_get = function () {
	var bitsize = ref.alloc(_binding.uint)
	var speed = ref.alloc(_binding.ulong)
	act(_binding.b0_ain_bitsize_speed_get(this._pointer, bitsize, speed))
	return {
		bitsize: bitsize.deref(),
		speed: speed.deref()
	}
}

Ain.prototype.chan_seq_set = function (list) {
	var uint_list = new _binding.UintArray(list)
	act(_binding.b0_ain_chan_seq_set(this._pointer, uint_list.buffer, list.length))
}

Ain.prototype.chan_seq_get = function () {
	var target = this.chan_count + 1

	while (true) {
		var list = new _binding.UintArray(target)
		var count = _binding.alloc_size_t(target)
		act(_binding.b0_ain_chan_seq_get(this._pointer, list.buffer, count))

		count = count.deref()
		if (count < target) {
			return _binding.arr_buffer_to_normal(list, count)
		}
	}
}

Ain.prototype.snapshot_prepare = function() {
	act(_binding.b0_ain_snapshot_prepare(this._pointer))
}

Ain.prototype.snapshot_start = function(arr) {
	var res = module.data_map_to_type(arr, {
		'void': _binding.b0_ain_snapshot_start,
		double: _binding.b0_ain_snapshot_start_double,
		float: _binding.b0_ain_snapshot_start_float
	})

	act(res.cb(this._pointer, res.data, res.count))
}

Ain.prototype.snapshot_stop = function() {
	act(_binding.b0_ain_snapshot_stop(this._pointer))
}

Ain.prototype.stream_prepare = function() {
	act(_binding.b0_ain_stream_prepare(this._pointer))
}

Ain.prototype.stream_start = function() {
	act(_binding.b0_ain_stream_start(this._pointer))
}

Ain.prototype.stream_read = function(arr, partialRead) {
	var res = module.data_map_to_type(arr, {
		'void': _binding.b0_ain_stream_read,
		double: _binding.b0_ain_stream_read_double,
		float: _binding.b0_ain_stream_read_float
	})

	partialRead = partialRead || false
	var readed_ptr = ref.NULL_POINTER
	if (partialRead) {
		readed_ptr = ref.alloc(ref.types.size_t)
	}

	act(res.cb(this._pointer, res.data, res.count, readed_ptr))
	return partialRead ? readed_ptr.deref() : res.count
}

Ain.prototype.stream_stop = function() {
	act(_binding.b0_ain_stream_stop(this._pointer))
}

exports.Ain = Ain
