/*
 * This file is part of node-box0.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * node-box0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * node-box0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with node-box0.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

var ref = require("ref")
var array = require("ref-array")
var _binding = require("../_binding.js")
var act = require("../exception.js").act
var module = require("./module.js")

var Pwm = function(device, index) {
	var pointer = ref.alloc(_binding.ptr_b0_pwm)
	act(_binding.b0_pwm_open(device._pointer, pointer, index))
	this._pointer = pointer.deref()

	var obj = this._pointer.deref()

	this.header = {
		type: obj.header.type,
		index: obj.header.index,
		name: obj.header.name,
		device: device
	}

	this.pin_count = obj.pin_count

	this.label = {
		pin: _binding.strings_to_array(obj.label.pin, obj.pin_count)
	}

	this.bitsize = _binding.as_uint_array(obj.speed.values, obj.speed.count)
	this.speed = _binding.as_ulong_array(obj.speed.values, obj.speed.count)

	this.ref = {
		low: obj.ref_.low,
		high: obj.ref_.high,
		type: obj.ref_.type,
	}
}

Pwm.prototype.speed_set = function (value) {
	act(_binding.b0_pwm_speed_set(this._pointer, value))
}

Pwm.prototype.speed_get = function () {
	var value = ref.alloc(_binding.ulong)
	act(_binding.b0_pwm_speed_get(this._pointer, value))
	return value.deref()
}

Pwm.prototype.bitsize_set = function (value) {
	act(_binding.b0_pwm_bitsize_set(this._pointer, value))
}

Pwm.prototype.bitsize_get = function () {
	var value = ref.alloc(_binding.uint)
	act(_binding.b0_pwm_bitsize_get(this._pointer, value))
	return value.deref()
}

Pwm.prototype.width_set = function(index, width) {
	act(_binding.b0_pwm_width_set(this._pointer, index, width))
}

Pwm.prototype.width_get = function(index) {
	var width = ref.alloc(_binding.b0_pwm_reg)
	act(_binding.b0_pwm_width_set(this._pointer, index, width))
	return width.deref()
}

Pwm.prototype.period_set = function(index, period) {
	act(_binding.b0_pwm_period_set(this._pointer, index, period))
}

Pwm.prototype.period_get = function() {
	var period = ref.alloc(_binding.b0_pwm_reg)
	act(_binding.b0_pwm_period_set(this._pointer, period))
	return period.deref()
}

Pwm.prototype.output_calc = function(bitsize, freq, max_error, best_result) {
	best_result = best_result || false
	var speed = ref.alloc(ref.types.ulong)
	var period = ref.alloc(_binding.b0_pwm_reg)
	act(_binding.b0_pwm_output_calc(this._pointer, bitsize, freq, speed,
					period, max_error, best_result))
	return {
		speed: speed.deref(),
		period: period.deref()
	}
}

Pwm.prototype.output_prepare = function () {
	act(_binding.b0_pwm_output_prepare(this._pointer))
}

Pwm.prototype.output_start = function() {
	act(_binding.b0_pwm_output_start(this._pointer))
}

Pwm.prototype.output_stop = function() {
	act(_binding.b0_pwm_output_stop(this._pointer))
}

Pwm.prototype.output_calc_width = function (period, duty_cycle) {
	return parseInt((period * duty_cycle) / 100.0)
}

Pwm.prototype.output_calc_duty_cycle = function (period, width) {
	return (width * 100.0) / period
}

Pwm.prototype.output_calc_freq = function (speed, period) {
	return speed / (1.0 * period)
}

Pwm.prototype.output_calc_freq_err = function (require_freq, calc_freq) {
	var df = Math.abs(require_freq - calc_freq)
	return (df * 100.0) / required_freq
}

exports.Pwm = Pwm
