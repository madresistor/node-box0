/*
 * This file is part of node-box0.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * node-box0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * node-box0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with node-box0.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

var ref = require("ref")
var array = require("ref-array")
var _binding = require("../_binding.js")
var act = require("../exception.js").act
var module = require("./module.js")

var bool = ref.types.bool
var arr_bool = array(bool)

var Pin = function(index, module) {
	this._module = module
	this._index = index
}

Pin.prototype.value = function(value) {
	if (value !== undefined) {
		this._module.value_set(this._index, value)
	} else {
		return this._module.value_get(this._index)
	}
}

Pin.prototype.high = function() { this.value(_binding.B0_DIO_HIGH) }
Pin.prototype.low = function() { this.value(_binding.B0_DIO_HIGH) }
Pin.prototype.toggle = function() { this._module.value_toggle(this._index) }

Pin.prototype.dir = function(value) {
	if (value !== undefined) {
		this._module.dir_set(this._index, value)
	} else {
		return this._module.dir_get(this._index)
	}
}

Pin.prototype.input = function() { this.dir(_binding.B0_DIO_INPUT) }
Pin.prototype.output = function() { this.dir(_binding.B0_DIO_OUTPUT) }

Pin.prototype.hiz = function(value) {
	if (value !== undefined) {
		this._module.hiz_set(this._index, value)
	} else {
		return this._module.hiz_get(this._index)
	}
}

Pin.prototype.enable = function() { this.hiz(_binding.B0_DIO_DISABLE) }
Pin.prototype.disable = function() { this.hiz(_binding.B0_DIO_ENABLE) }

var Dio = function(device, index) {
	var pointer = ref.alloc(_binding.ptr_b0_dio)
	act(_binding.b0_dio_open(device._pointer, pointer, index))
	this._pointer = pointer.deref()

	var obj = this._pointer.deref()

	this.header = {
		type: obj.header.type,
		index: obj.header.index,
		name: obj.header.name,
		device: device
	}

	this.pin_count = obj.pin_count
	this.capab = obj.capab

	this.label = {
		pin: _binding.strings_to_array(obj.label.pin, obj.pin_count)
	}

	this.ref = {
		low: obj.ref_.low,
		high: obj.ref_.high,
		type: obj.ref_.type,
	}
}

Dio.prototype.basic_prepare = function () {
	act(_binding.b0_dio_basic_prepare(this._pointer))
}

Dio.prototype.basic_start = function () {
	act(_binding.b0_dio_basic_start(this._pointer))
}

Dio.prototype.basic_stop = function () {
	act(_binding.b0_dio_basic_stop(this._pointer))
}

Dio.prototype.pin = function(index) {
	return new Pin(index, this)
}

function single_get(fn) {
	return function(index) {
		var value = ref.alloc(bool)
		act(fn(this._pointer, index, value))
		return value.deref()
	}
}

function single_set(fn) {
	return function(index, value) {
		act(fn(this._pointer, index, value))
	}
}

Dio.prototype.value_get = single_get(_binding.b0_dio_value_get)
Dio.prototype.value_set = single_set(_binding.b0_dio_value_set)

Dio.prototype.value_toggle = function(index) {
	act(_binding.b0_dio_value_toggle(this._pointer, index))
}

Dio.prototype.dir_set = single_set(_binding.b0_dio_dir_set)
Dio.prototype.dir_get = single_get(_binding.b0_dio_dir_get)

Dio.prototype.hiz_set = single_set(_binding.b0_dio_hiz_set)
Dio.prototype.hiz_get = single_get(_binding.b0_dio_hiz_get)

function multiple_get(fn) {
	return function(indexes) {
		var buf = new Buffer(indexes)
		var ret = arr_bool(buf.length)
		act(fn(this._pointer, buf, ret, buf.length))
		return _binding.arr_buffer_to_normal(ret)
	}
}

function multiple_set(fn) {
	return function(indexes, value) {
		var buf = new Buffer(indexes)
		act(fn(this._pointer, buf, buf.length, value))
	}
}

Dio.prototype.multiple_value_get = multiple_get(_binding.b0_dio_multiple_value_get)
Dio.prototype.multiple_value_set = multiple_set(_binding.b0_dio_multiple_value_set)

Dio.prototype.multiple_value_toggle = function(indexes) {
	var buf = new Buffer(indexes)
	act(_binding.b0_dio_multiple_value_toggle(this._pointer, buf, buf.length))
}

Dio.prototype.multiple_dir_get = multiple_get(_binding.b0_dio_multiple_dir_get)
Dio.prototype.multiple_dir_set = multiple_set(_binding.b0_dio_multiple_dir_set)

Dio.prototype.multiple_hiz_get = multiple_get(_binding.b0_dio_multiple_hiz_get)
Dio.prototype.multiple_hiz_set = multiple_set(_binding.b0_dio_multiple_hiz_set)

function all_set(fn) {
	return function(value) {
		act(fn(this._pointer, value))
	}
}

function all_get(fn) {
	return function(value) {
		var ret = arr_bool(this.count.value)
		act(fn(this._pointer, ret))
		return _binding.arr_buffer_to_normal(ret)
	}
}

Dio.prototype.all_value_get = all_get(_binding.b0_dio_all_value_get)
Dio.prototype.all_value_set = all_set(_binding.b0_dio_all_value_set)

Dio.prototype.all_value_toggle = function() {
	act(_binding.b0_dio_all_value_toggle(this._pointer))
}

Dio.prototype.all_dir_get = all_get(_binding.b0_dio_all_dir_get)
Dio.prototype.all_dir_set = all_set(_binding.b0_dio_all_dir_set)

Dio.prototype.all_hiz_get = all_get(_binding.b0_dio_all_hiz_get)
Dio.prototype.all_hiz_set = all_set(_binding.b0_dio_all_hiz_set)

exports.Dio = Dio
