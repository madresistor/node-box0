/*
 * This file is part of node-box0.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * node-box0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * node-box0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with node-box0.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

var _binding = require("../_binding.js")
var act = require("../exception.js").act

var Ain = require("./ain.js").Ain
var Dio = require("./dio.js").Dio
var Aout = require("./aout.js").Aout

var module_clazz_lookup = {}
module_clazz_lookup[_binding.B0_DIO] = Dio
module_clazz_lookup[_binding.B0_AOUT] = Aout
module_clazz_lookup[_binding.B0_AIN] = Ain
//~ module_clazz_lookup[_binding.B0_SPI] = Spi
//~ module_clazz_lookup[_binding.B0_I2C] = I2c
//~ module_clazz_lookup[_binding.B0_PWM] = Pwm
//~ module_clazz_lookup[_binding.B0_UNIO] = Unio

var Module = function (pointer, device) {
	var obj = pointer.deref()
	this.type = obj.type
	this.index = obj.index
	this.name = obj.name
	this.device = device
	this._pointer = pointer
}

Module.prototype.info = function () {
	act(_binding.b0_module_info(this._pointer))
}

Module.prototype.openable = function() {
	var r = _binding.b0_module_openable(this._pointer)
	if (r == _binding.B0_ERR_UNAVAIL) {
		return false
	} else if (r >= _binding.B0_OK) {
		return true
	}

	act(r)
}

Module.prototype.open = function() {
	var clazz = module_clazz_lookup[this.type]
	if (_clazz === undefined) {
		throw new Error("module not supported")
	}

	return _clazz(this.device, this.index)
}

/* assuming that the argument @a arr is a Buffer,
 *  if you want to pass a Uint16Array or like something,
 *  use the same trick as below (used for float32 and float64)
 *  and use TypedArray.slice() to pass a sub-set of the array */
function data_map_to_type(arr, cb) {
	var cb_fn = null

	if (arr.constructor == Float32Array) {
		cb_fn = cb.float
	} else if (arr.constructor == Float64Array) {
		cb_fn = cb.double
	}

	if (cb_fn == null) {
		return {
			cb: cb.void,
			data: arr,
			count: arr.byteLength
		}
	} else {
		return {
			cb: cb_fn,
			data: new Buffer(arr.buffer),
			count: arr.length
		}
	}
}

exports.Module = Module
exports.data_map_to_type = data_map_to_type
