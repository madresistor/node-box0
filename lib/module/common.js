/*
 * This file is part of node-box0.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * node-box0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * node-box0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with node-box0.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

/* just to remove the common code */

var _binding = require("../_binding.js")
var act = require('../exception.js').act

function close() {
	if (this._pointer == null) {
		throw new Error("Module already closed?")
	}

	act(this._close(this._pointer))
	this._pointer = null
}

function to_class_name(str) {
	/* convert first character to uppercase */
	return str.charAt(0).toUpperCase() + str.substring(1)
}

var mod_list = ["ain", "aout", "dio", "pwm"]

for (var name of mod_list) {
	var Clazz = require("./" + name + ".js")[to_class_name(name)]

	var prefix = 'b0_' + name + '_';
	Clazz.prototype._close = _binding[prefix + 'close']
	Clazz.prototype.close = close
}
