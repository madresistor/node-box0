/*
 * This file is part of node-box0.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * node-box0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * node-box0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with node-box0.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

var _backend = require('./_binding.js')
var ref = require('ref')

function version() {
	var ret = new _backend.b0_version
	_backend.b0_version_extract(ret.ref())
	/* http://semver.org/ */
	return 'v' + ret.major + '.' + ret.minor + '.' + ret.patch
}

exports.version = version
