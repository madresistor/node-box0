/*
 * This file is part of node-box0.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * node-box0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * node-box0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with node-box0.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

var ref = require('ref')
var _binding = require('../../_binding.js')
var act = require('../../exception.js').act

exports.PM5 = _binding.B0V5_PS_PM5
exports.P3V3 = _binding.B0V5_PS_P3V3

exports.ANALOG = _binding.B0V5_PS_ANALOG
exports.DIGITAL = _binding.B0V5_PS_DIGITAL


function en_set(mask, value) {
	act(_binding.b0v5_ps_en_set(this._pointer, mask, value))
}

function en_get() {
	var value = ref.alloc(ref.types.uint8)
	act(_binding.b0v5_ps_en_get(this._pointer, value))
	return value.deref()
}

function oc_get() {
	var value = ref.alloc(ref.types.uint8)
	act(_binding.b0v5_ps_oc_get(this._pointer, value))
	return value.deref();
}

function oc_ack(value) {
	act(_binding.b0v5_ps_oc_ack(this._pointer, value))
}

exports.en_set = en_set
exports.en_get = en_get
exports.oc_get = oc_get
exports.oc_ack = oc_ack
