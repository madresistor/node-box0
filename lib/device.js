/*
 * This file is part of node-box0.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * node-box0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * node-box0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with node-box0.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

var ref = require("ref")
var _binding = require("./_binding.js")
var Module = require("./module/module.js").Module
var act = require("./exception.js").act
var Ain = require("./module/ain.js").Ain
var Aout = require("./module/aout.js").Aout
var Dio = require("./module/dio.js").Dio
var Pwm = require("./module/pwm.js").Pwm

/* blog post that saved the binding!
 *  http://pixomania.net/programming/complex-data-structures-with-node-ffi/
 */

var Device = function (pointer) {
	var obj = pointer.deref()

	this.modules = []
	this.name = obj.name
	this.manuf = obj.manuf
	this.serial = obj.serial
	this._pointer = pointer

	for (var i = 0; i < obj.modules_len; i++) {
		var byte_offset = i * ref.sizeof.pointer;
		var mod_ptr = ref.get(obj.modules, byte_offset, _binding.ptr_b0_module)
		this.modules.push(new Module(mod_ptr, this))
	}
}

Device.prototype.close = function() {
	if (this._pointer == null) {
		throw Error("Device is already closed?")
	}

	act(_binding.b0_device_close(this._pointer))
	this._pointer = null
}

Device.prototype.ping = function() {
	act(_binding.b0_device_ping(this._pointer))
}

Device.prototype.log = function(log_level) {
	act(_binding.b0_device_log(this._pointer, log_level))
}

Device.prototype.ain = function(i) { return new Ain(this, i || 0) }
Device.prototype.aout = function(i) { return new Aout(this, i || 0) }
Device.prototype.dio = function(i) { return new Dio(this, i || 0) }
Device.prototype.pwm = function(i) { return new Pwm(this, i || 0) }

exports.Device = Device
