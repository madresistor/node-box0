/*
 * This file is part of node-box0.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * node-box0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * node-box0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with node-box0.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

var ref = require('ref')
var ffi = require('ffi')
var struct = require('ref-struct')
var ArrayType = require('ref-array')


var uint8 = ref.types.uint8
var ptr_uint8 = ref.refType(uint8)
var uint32 = ref.types.uint32
var ptr_uint32 = ref.refType(uint32)
var ulonglong = ref.types.ulonglong
var size_t = ref.types.size_t
var ptr_size_t = ref.refType(size_t)
var int = ref.types.int
var uint = ref.types.uint
var ptr_uint = ref.refType(uint)
var ulong = ref.types.ulong
var ptr_ulong = ref.refType(ulong)
var void_ = ref.types.void
var ptr_void = ref.refType(void_)
var double = ref.types.double
var ptr_double = ref.refType(double)
var float = ref.types.float
var ptr_float = ref.refType(float)
var b0_str = ref.types.CString
var ptr_b0_str = ref.refType(b0_str)
var bool = ref.types.bool
var ptr_bool = ref.refType(bool)

exports.uint = uint
exports.ulong = ulong

var b0_module_type = int
var b0_result_code = int
var b0_log_level = int
var b0_ref_type = int
var enum_ = int

var b0_version = struct({
	major: uint8,
	minor: uint8,
	patch: uint8
})

var ptr_b0_version = ref.refType(b0_version)

var b0_module = struct({
	type: b0_module_type,
	index: int,
	name: b0_str,
	/* cyclic dependency, see below {
	device: ptr_b0_device,
	backend_data: ptr_void,
	frontend_data: ptr_void
	} */
})

var ptr_b0_module = ref.refType(b0_module)
var ptr_ptr_b0_module = ref.refType(ptr_b0_module)

var b0_backend = void_
var ptr_b0_backend = ref.refType(b0_backend)

var b0_device = struct({
	modules_len: size_t,
	modules: ptr_ptr_b0_module,
	name: b0_str,
	manuf: b0_str,
	serial: b0_str,

	/* no use  { */
	backend_data: ptr_void,
	frontend_data: ptr_void,
	backend: ptr_b0_backend
	/* } */
})

var ptr_b0_device = ref.refType(b0_device)
var ptr_ptr_b0_device = ref.refType(ptr_b0_device)

/* cyclic dependency of device, module */
b0_module.defineProperty('device', ptr_b0_device)
b0_module.defineProperty('backend_data', ptr_void)
b0_module.defineProperty('frontend_data', ptr_void)

var b0_ain_bitsize_speeds = struct({
	bitsize: uint,
	speed: struct({
		values: ptr_ulong,
		count: size_t
	})
})

var ptr_b0_ain_bitsize_speeds = ref.refType(b0_ain_bitsize_speeds)

var b0_ain_mode = struct({
	values: ptr_b0_ain_bitsize_speeds,
	count: size_t
})

var b0_ain = struct({
	header: b0_module,
	chan_count: uint,
	buffer_size: size_t,
	capab: enum_,
	label: struct({
		chan: ptr_b0_str
	}),
	ref_: struct({
		high: double,
		low: double,
		type: enum_
	}),
	stream: b0_ain_mode,
	snapshot: b0_ain_mode,
})

var ptr_b0_ain = ref.refType(b0_ain)
var ptr_ptr_b0_ain = ref.refType(ptr_b0_ain)

var b0_aout_bitsize_speeds = struct({
	bitsize: uint,
	speed: struct({
		values: ptr_ulong,
		count: size_t
	})
})

var ptr_b0_aout_bitsize_speeds = ref.refType(b0_aout_bitsize_speeds)

var b0_aout_mode = struct({
	values: ptr_b0_aout_bitsize_speeds,
	count: size_t
})

var b0_aout = struct({
	header: b0_module,
	chan_count: uint,
	buffer_size: size_t,
	capab: enum_,
	label: struct({
		chan: ptr_b0_str
	}),
	ref_: struct({
		high: double,
		low: double,
		type: enum_
	}),
	stream: b0_aout_mode,
	snapshot: b0_aout_mode
})

var ptr_b0_aout = ref.refType(b0_aout)
var ptr_ptr_b0_aout = ref.refType(ptr_b0_aout)

var b0_dio = struct({
	header: b0_module,
	pin_count: uint,
	capab: enum_,
	label: struct({
		pin: ptr_b0_str
	}),
	ref_: struct({
		high: double,
		low: double,
		type: enum_
	})
})

var ptr_b0_dio = ref.refType(b0_dio)
var ptr_ptr_b0_dio = ref.refType(ptr_b0_dio)

var b0_pwm = struct({
	header: b0_module,
	pin_count: uint,
	label: struct({
		pin: ptr_b0_str
	}),
	bitsize: struct({
		values: uint,
		count: size_t
	}),
	speed: struct({
		values: ptr_ulong,
		count: size_t
	}),
	ref_: struct({
		high: double,
		low: double,
		type: enum_
	})
})

var b0_pwm_reg = ulonglong
var ptr_b0_pwm_reg = ref.refType(b0_pwm_reg)
var ptr_b0_pwm = ref.refType(b0_pwm)
var ptr_ptr_b0_pwm = ref.refType(ptr_b0_pwm)

var libusb_context = void_
var ptr_libusb_context = ref.refType(libusb_context)
var ptr_ptr_libusb_context = ref.refType(ptr_libusb_context)

var libusb_device = void_
var ptr_libusb_device = ref.refType(libusb_device)
var ptr_ptr_libusb_device = ref.refType(ptr_libusb_device)

var libusb_device_handle = void_
var ptr_libusb_device_handle = ref.refType(libusb_device_handle)
var ptr_ptr_libusb_device_handle = ref.refType(ptr_libusb_device_handle)

var func_def = {}

var mod_list = {b0_ain, b0_aout, b0_dio, b0_pwm}

for(var name in mod_list) {
	var type = mod_list[name]
	var ptr_type = ref.refType(type)
	var ptr_ptr_type = ref.refType(ptr_type)
	var sig1 = [b0_result_code, [ptr_type]]
	var sig2 = [b0_result_code, [ptr_b0_device, ptr_ptr_type, int]]

	func_def[name + '_close'] = sig1
	func_def[name + '_open'] = sig2

	exports[name] = type
	exports['ptr_' + name] = ptr_type
}

exports.b0_ain_bitsize_speeds = b0_ain_bitsize_speeds
exports.ptr_b0_ain_bitsize_speeds = ptr_b0_ain_bitsize_speeds
exports.b0_aout_bitsize_speeds = b0_aout_bitsize_speeds
exports.ptr_b0_aout_bitsize_speeds = ptr_b0_aout_bitsize_speeds

var libbox0 = ffi.Library("libbox0", Object.assign(func_def, {
	b0_version_extract: [uint32, [ptr_b0_version]],
	b0_result_name: [b0_str, [b0_result_code]],
	b0_result_explain: [b0_str, [b0_result_code]],

	/* usb */
	b0_usb_open_supported: [b0_result_code, [ptr_ptr_b0_device]],
	b0_usb_libusb_device: [b0_result_code, [ptr_b0_device, ptr_ptr_libusb_device]],
	b0_usb_libusb_context: [b0_result_code, [ptr_b0_device, ptr_ptr_libusb_context]],
	b0_usb_libusb_device_handle: [b0_result_code, [ptr_b0_device, ptr_ptr_libusb_device_handle]],

	b0_module_openable: [b0_result_code, [ptr_b0_module]],

	b0_device_close: [b0_result_code, [ptr_b0_device]],

	/* ain */
	b0_ain_bitsize_speed_set: [b0_result_code, [ptr_b0_ain, uint, ulong]],
	b0_ain_bitsize_speed_get: [b0_result_code, [ptr_b0_ain, ptr_uint, ptr_ulong]],
	b0_ain_chan_seq_set: [b0_result_code, [ptr_b0_ain, ptr_uint, size_t]],
	b0_ain_chan_seq_get: [b0_result_code, [ptr_b0_ain, ptr_uint, ptr_size_t]],

	/* ain   stream */
	b0_ain_stream_prepare: [b0_result_code, [ptr_b0_ain]],
	b0_ain_stream_start: [b0_result_code, [ptr_b0_ain]],
	b0_ain_stream_read: [b0_result_code, [ptr_b0_ain, ptr_void, size_t, ptr_size_t]],
	b0_ain_stream_read_float: [b0_result_code, [ptr_b0_ain, ptr_float, size_t, ptr_size_t]],
	b0_ain_stream_read_double: [b0_result_code, [ptr_b0_ain, ptr_double, size_t, ptr_size_t]],
	b0_ain_stream_stop: [b0_result_code, [ptr_b0_ain]],

	/* ain   snapshot */
	b0_ain_snapshot_prepare: [b0_result_code, [ptr_b0_ain]],
	b0_ain_snapshot_start: [b0_result_code, [ptr_b0_ain, ptr_void, size_t]],
	b0_ain_snapshot_start_double: [b0_result_code, [ptr_b0_ain, ptr_double, size_t]],
	b0_ain_snapshot_start_float: [b0_result_code, [ptr_b0_ain, ptr_float, size_t]],
	b0_ain_snapshot_stop: [b0_result_code, [ptr_b0_ain]],

	/* aout */
	b0_aout_bitsize_speed_set: [b0_result_code, [ptr_b0_aout, uint, ulong]],
	b0_aout_bitsize_speed_get: [b0_result_code, [ptr_b0_aout, ptr_uint, ptr_ulong]],
	b0_aout_chan_seq_set: [b0_result_code, [ptr_b0_aout, ptr_uint, size_t]],
	b0_aout_chan_seq_get: [b0_result_code, [ptr_b0_aout, ptr_uint, ptr_size_t]],
	b0_aout_repeat_set: [b0_result_code, [ptr_b0_aout, ulong]],
	b0_aout_repeat_get: [b0_result_code, [ptr_b0_aout, ptr_ulong]],

	/* aout   stream*/
	b0_aout_stream_prepare: [b0_result_code, [ptr_b0_aout]],
	b0_aout_stream_start: [b0_result_code, [ptr_b0_aout]],
	b0_aout_stream_stop: [b0_result_code, [ptr_b0_aout]],
	b0_aout_stream_write: [b0_result_code, [ptr_b0_aout, ptr_void, size_t]],
	b0_aout_stream_write_double: [b0_result_code, [ptr_b0_aout, ptr_double, size_t]],
	b0_aout_stream_write_float: [b0_result_code, [ptr_b0_aout, ptr_float, size_t]],

	/* aout   snapshot */
	b0_aout_snapshot_prepare: [b0_result_code, [ptr_b0_aout]],
	b0_aout_snapshot_start: [b0_result_code, [ptr_b0_aout, ptr_void, size_t]],
	b0_aout_snapshot_start_float: [b0_result_code, [ptr_b0_aout, ptr_float, size_t]],
	b0_aout_snapshot_start_double: [b0_result_code, [ptr_b0_aout, ptr_double, size_t]],
	b0_aout_snapshot_stop: [b0_result_code, [ptr_b0_aout]],
	b0_aout_snapshot_calc: [b0_result_code, [ptr_b0_aout, double, uint, ptr_size_t, ptr_ulong]],

	/* dio */
	b0_dio_basic_prepare: [b0_result_code, [ptr_b0_dio]],
	b0_dio_basic_start: [b0_result_code, [ptr_b0_dio]],
	b0_dio_basic_stop: [b0_result_code, [ptr_b0_dio]],

	/* dio   single */
	b0_dio_value_get: [b0_result_code, [ptr_b0_dio, uint, ptr_bool]],
	b0_dio_value_set: [b0_result_code, [ptr_b0_dio, uint, bool]],
	b0_dio_value_toggle: [b0_result_code, [ptr_b0_dio, uint]],

	b0_dio_dir_get: [b0_result_code, [ptr_b0_dio, uint, ptr_bool]],
	b0_dio_dir_set: [b0_result_code, [ptr_b0_dio, uint, bool]],

	b0_dio_hiz_get: [b0_result_code, [ptr_b0_dio, uint, ptr_bool]],
	b0_dio_hiz_set: [b0_result_code, [ptr_b0_dio, uint, bool]],

	/* dio   multiple */
	b0_dio_multiple_value_set: [b0_result_code, [ptr_b0_dio, ptr_uint, size_t, bool]],
	b0_dio_multiple_value_get: [b0_result_code, [ptr_b0_dio, ptr_uint, ptr_bool, size_t]],
	b0_dio_multiple_value_toggle: [b0_result_code, [ptr_b0_dio, ptr_uint, size_t]],

	b0_dio_multiple_dir_set: [b0_result_code, [ptr_b0_dio, ptr_uint, size_t, bool]],
	b0_dio_multiple_dir_get: [b0_result_code, [ptr_b0_dio, ptr_uint, ptr_bool, size_t]],

	b0_dio_multiple_hiz_set: [b0_result_code, [ptr_b0_dio, ptr_uint, size_t, bool]],
	b0_dio_multiple_hiz_get: [b0_result_code, [ptr_b0_dio, ptr_uint, ptr_bool, size_t]],

	/* dio   all */
	b0_dio_all_value_set: [b0_result_code, [ptr_b0_dio, bool]],
	b0_dio_all_value_get: [b0_result_code, [ptr_b0_dio, ptr_bool]],
	b0_dio_all_value_toggle: [b0_result_code, [ptr_b0_dio]],

	b0_dio_all_dir_set: [b0_result_code, [ptr_b0_dio, bool]],
	b0_dio_all_dir_get: [b0_result_code, [ptr_b0_dio, ptr_bool]],

	b0_dio_all_hiz_set: [b0_result_code, [ptr_b0_dio, bool]],
	b0_dio_all_hiz_get: [b0_result_code, [ptr_b0_dio, ptr_bool]],

	/* pwm */
	b0_pwm_width_set: [b0_result_code, [ptr_b0_pwm, uint, b0_pwm_reg]],
	b0_pwm_width_get: [b0_result_code, [ptr_b0_pwm, uint, ptr_b0_pwm_reg]],
	b0_pwm_period_set: [b0_result_code, [ptr_b0_pwm, b0_pwm_reg]],
	b0_pwm_period_get: [b0_result_code, [ptr_b0_pwm, ptr_b0_pwm_reg]],

	b0_pwm_speed_set: [b0_result_code, [ptr_b0_pwm, ulong]],
	b0_pwm_speed_get: [b0_result_code, [ptr_b0_pwm, ptr_ulong]],
	b0_pwm_bitsize_set: [b0_result_code, [ptr_b0_pwm, uint]],
	b0_pwm_bitsize_get: [b0_result_code, [ptr_b0_pwm, ptr_uint]],

	/* pwm output */
	b0_pwm_output_prepare: [b0_result_code, [ptr_b0_pwm]],
	b0_pwm_output_calc: [b0_result_code, [ptr_b0_pwm, uint, double, ptr_ulong, ptr_b0_pwm_reg, double, bool]],
	b0_pwm_output_start: [b0_result_code, [ptr_b0_pwm]],
	b0_pwm_output_stop: [b0_result_code, [ptr_b0_pwm]],

	/* box0-v5 */
	b0v5_valid_test: [b0_result_code, [ptr_b0_device]],

	b0v5_ps_en_set: [b0_result_code, [ptr_b0_device, uint8, uint8]],
	b0v5_ps_en_get: [b0_result_code, [ptr_b0_device, ptr_uint8]],
	b0v5_ps_oc_get: [b0_result_code, [ptr_b0_device, ptr_uint8]],
	b0v5_ps_oc_ack: [b0_result_code, [ptr_b0_device, uint8]],
}))

/* export all libbox0 functions */
for (var i in libbox0) {
	exports[i] = libbox0[i]
}

/* types */
exports.b0_module_type = b0_module_type
exports.b0_result_code = b0_result_code
exports.ptr_b0_version = ptr_b0_version
exports.b0_version = b0_version
exports.b0_module = b0_module
exports.ptr_b0_module = ptr_b0_module
exports.ptr_ptr_b0_module = ptr_ptr_b0_module
exports.b0_backend = b0_backend
exports.ptr_b0_backend = ptr_b0_backend
exports.b0_device = b0_device
exports.ptr_b0_device = ptr_b0_device
exports.ptr_ptr_b0_device = ptr_ptr_b0_device
exports.b0_log_level = b0_log_level

exports.b0_pwm_reg = b0_pwm_reg

exports.libusb_context = libusb_context
exports.ptr_libusb_context = ptr_libusb_context
exports.ptr_ptr_libusb_context = ptr_ptr_libusb_context
exports.libusb_device = libusb_device
exports.ptr_libusb_device = ptr_libusb_device
exports.ptr_ptr_libusb_device = ptr_ptr_libusb_device
exports.libusb_device_handle = libusb_device_handle
exports.ptr_libusb_device_handle = ptr_libusb_device_handle
exports.ptr_ptr_libusb_device_handle = ptr_ptr_libusb_device_handle

/* constants */
exports.B0_LOG_NONE = 0
exports.B0_LOG_ERROR = 1
exports.B0_LOG_WARN = 2
exports.B0_LOG_INFO = 3
exports.B0_LOG_DEBUG = 4

exports.B0_FAIL_CALC = -14
exports.B0_ERR_STATE = -13
exports.B0_ERR_UNDERFLOW = -12
exports.B0_ERR_OVERFLOW = -11
exports.B0_ERR_UNAVAIL = -10
exports.B0_ERR_TIMEOUT = -9
exports.B0_ERR_BUSY = -8
exports.B0_ERR_BOGUS = -7
exports.B0_BUG = -6
exports.B0_ERR_IO = -5
exports.B0_ERR_ARG = -4
exports.B0_ERR_SUPP = -3
exports.B0_ERR_ALLOC = -2
exports.B0_ERR = -1
exports.B0_OK = 0

exports.B0_DIO = 1
exports.B0_AOUT = 2
exports.B0_AIN = 3
exports.B0_SPI = 4
exports.B0_I2C = 5
exports.B0_PWM = 6

exports.B0_AIN_CAPAB_FORMAT_2COMPL = (1 << 0)
exports.B0_AIN_CAPAB_FORMAT_BINARY = (0 << 0)
exports.B0_AIN_CAPAB_ALIGN_LSB = (0 << 1)
exports.B0_AIN_CAPAB_ALIGN_MSB = (1 << 1)
exports.B0_AIN_CAPAB_ENDIAN_LITTLE = (0 << 2)
exports.B0_AIN_CAPAB_ENDIAN_BIG = (1 << 2)

exports.B0_AOUT_CAPAB_FORMAT_2COMPL = (1 << 0)
exports.B0_AOUT_CAPAB_FORMAT_BINARY = (0 << 0)
exports.B0_AOUT_CAPAB_ALIGN_LSB = (0 << 1)
exports.B0_AOUT_CAPAB_ALIGN_MSB = (1 << 1)
exports.B0_AOUT_CAPAB_ENDIAN_LITTLE = (0 << 2)
exports.B0_AOUT_CAPAB_ENDIAN_BIG = (1 << 2)
exports.B0_AOUT_CAPAB_REPEAT = (1 << 3)

exports.B0_DIO_CAPAB_OUTPUT = (1 << 0)
exports.B0_DIO_CAPAB_INPUT = (1 << 1)
exports.B0_DIO_CAPAB_HIZ = (1 << 2)

exports.B0_REF_VOLTAGE = 0
exports.B0_REF_CURRENT = 1

exports.B0_I2C_VERSION_SM = 0
exports.B0_I2C_VERSION_FM = 1
exports.B0_I2C_VERSION_HS = 2
exports.B0_I2C_VERSION_HS_CLEANUP1 = 3
exports.B0_I2C_VERSION_FMPLUS = 4
exports.B0_I2C_VERSION_UFM = 5
exports.B0_I2C_VERSION_VER5 = 6
exports.B0_I2C_VERSION_VER6 = 7

exports.B0_DIO_LOW = false
exports.B0_DIO_HIGH = true

exports.B0_DIO_INPUT = false
exports.B0_DIO_OUTPUT = true

exports.B0_DIO_DISABLE = false
exports.B0_DIO_ENABLE = true

exports.B0V5_PS_PM5 = 0x01 /* ±5V power supply */
exports.B0V5_PS_P3V3 = 0x02 /* +3.3V power supply */

exports.B0V5_PS_ANALOG = exports.B0V5_PS_PM5
exports.B0V5_PS_DIGITAL = exports.B0V5_PS_P3V3

/**
 * Allocate a size_t and assign @a val to it
 * @param val Inital value
 */
function alloc_size_t(val) {
	var res = ref.alloc(size_t)
	var bits = ref.sizeof.size_t * 8
	ref['writeUInt' + bits](res, 0, val)
	return res
}

exports.alloc_size_t = alloc_size_t

function alloc_double(value) {
	var m = ref.alloc(ref.types.double)
	m['writeDouble' + exports.endianness](value)
	return m
}

exports.alloc_double = alloc_double

/**
 * Convert a Buffer based array to normal array (original buffer unmodified)
 * @param buf Buffer to use for conversion
 * @param len If not provided, full length is assumed
 */
function arr_buffer_to_normal(buf, len) {
	return Array.prototype.slice.call(buf, 0, len || buf.length)
}

exports.arr_buffer_to_normal = arr_buffer_to_normal

/** Convert a @a str_array with count @a count to
 *  native string array
 * @param str_array uint8_t ** string array
 * @param count Number of entries in @a str_array
 * @return Javascript array with string
 */
function strings_to_array(str_arr, count) {
	var result = []

	for (var i = 0; i < count; i++) {
		var byte_offset = i * ref.sizeof.pointer;
		var str = ref.get(str_arr, byte_offset, b0_str)
		result.push(str)
	}

	return result
}

exports.strings_to_array = strings_to_array

var UintArray = ArrayType(uint)

/**
 * Convert a ref-struct @a values to a unsigned int array with
 *  @a count items
 */
function as_uint_array(values, count) {
	var reint = values.reinterpret(count * ref.sizeof.uint)
	return UintArray(reint, count)
}

exports.UintArray = UintArray
exports.as_uint_array = as_uint_array

var UlongArray = ArrayType(ulong)

/**
 * Convert a ref-struct @a values to a unsigned long array with
 *  @a count items
 */
function as_ulong_array(values, count) {
	var reint = values.reinterpret(count * ref.sizeof.ulong)
	return UlongArray(reint, count)
}

exports.UlongArray = UlongArray
exports.as_ulong_array = as_ulong_array
