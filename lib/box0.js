/*
 * This file is part of node-box0.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * node-box0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * node-box0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with node-box0.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

var _binding = require("./_binding.js")
var extra = require('./extra.js')
var usb = require('./backend/usb.js')

/* common code execute */
require("./module/common.js")

module.exports = {
	version: extra.version,
	usb: usb,

	LOG_NONE: _binding.B0_LOG_NONE,
	LOG_ERROR: _binding.B0_LOG_ERROR,
	LOG_WARN: _binding.B0_LOG_WARN,
	LOG_INFO: _binding.B0_LOG_INFO,
	LOG_DEBUG: _binding.B0_LOG_DEBUG,

	DIO: _binding.B0_DIO,
	AOUT: _binding.B0_AOUT,
	AIN: _binding.B0_AIN,
	SPI: _binding.B0_SPI,
	I2C: _binding.B0_I2C,
	PWM: _binding.B0_PWM,

	FAIL_CALC: _binding.B0_FAIL_CALC,
	ERR_STATE: _binding.B0_ERR_STATE,
	ERR_UNDERFLOW: _binding.B0_ERR_UNDERFLOW,
	ERR_OVERFLOW: _binding.B0_ERR_OVERFLOW,
	ERR_UNAVAIL: _binding.B0_ERR_UNAVAIL,
	ERR_TIMEOUT: _binding.B0_ERR_TIMEOUT,
	ERR_BUSY: _binding.B0_ERR_BUSY,
	ERR_BOGUS: _binding.B0_ERR_BOGUS,
	BUG: _binding.B0_BUG,
	ERR_IO: _binding.B0_ERR_IO,
	ERR_ARG: _binding.B0_ERR_ARG,
	ERR_SUPP: _binding.B0_ERR_SUPP,
	ERR_ALLOC: _binding.B0_ERR_ALLOC,
	ERR: _binding.B0_ERR,
	OK: _binding.B0_OK,

	AIN_CAPAB_FORMAT_2COMPL: _binding.B0_AIN_CAPAB_FORMAT_2COMPL,
	AIN_CAPAB_FORMAT_BINARY: _binding.B0_AIN_CAPAB_FORMAT_BINARY,
	AIN_CAPAB_ALIGN_LSB: _binding.B0_AIN_CAPAB_ALIGN_LSB,
	AIN_CAPAB_ALIGN_MSB: _binding.B0_AIN_CAPAB_ALIGN_MSB,
	AIN_CAPAB_ENDIAN_LITTLE: _binding.B0_AIN_CAPAB_ENDIAN_LITTLE,
	AIN_CAPAB_ENDIAN_BIG: _binding.B0_AIN_CAPAB_ENDIAN_BIG,

	AOUT_CAPAB_FORMAT_2COMPL: _binding.B0_AOUT_CAPAB_FORMAT_2COMPL,
	AOUT_CAPAB_FORMAT_BINARY: _binding.B0_AOUT_CAPAB_FORMAT_BINARY,
	AOUT_CAPAB_ALIGN_LSB: _binding.B0_AOUT_CAPAB_ALIGN_LSB,
	AOUT_CAPAB_ALIGN_MSB: _binding.B0_AOUT_CAPAB_ALIGN_MSB,
	AOUT_CAPAB_ENDIAN_LITTLE: _binding.B0_AOUT_CAPAB_ENDIAN_LITTLE,
	AOUT_CAPAB_ENDIAN_BIG: _binding.B0_AOUT_CAPAB_ENDIAN_BIG,
	AOUT_CAPAB_REPEAT: _binding.B0_AOUT_CAPAB_REPEAT,

	DIO_CAPAB_OUTPUT: _binding.B0_DIO_CAPAB_OUTPUT,
	DIO_CAPAB_INPUT: _binding.B0_DIO_CAPAB_INPUT,
	DIO_CAPAB_HIZ: _binding.B0_DIO_CAPAB_HIZ,

	REF_VOLTAGE: _binding.B0_REF_VOLTAGE,
	REF_CURRENT: _binding.B0_REF_CURRENT,

	I2C_VERSION_SM: _binding.B0_I2C_VERSION_SM,
	I2C_VERSION_FM: _binding.B0_I2C_VERSION_FM,
	I2C_VERSION_HS: _binding.B0_I2C_VERSION_HS,
	I2C_VERSION_HS_CLEANUP1: _binding.B0_I2C_VERSION_HS_CLEANUP1,
	I2C_VERSION_FMPLUS: _binding.B0_I2C_VERSION_FMPLUS,
	I2C_VERSION_UFM: _binding.B0_I2C_VERSION_UFM,
	I2C_VERSION_VER5: _binding.B0_I2C_VERSION_VER5,
	I2C_VERSION_VER6: _binding.B0_I2C_VERSION_VER6,

	DIO_LOW: _binding.B0_DIO_LOW,
	DIO_HIGH: _binding.B0_DIO_HIGH,
	DIO_INPUT: _binding.B0_DIO_INPUT,
	DIO_OUTPUT: _binding.B0_DIO_OUTPUT,
	DIO_DISABLE: _binding.B0_DIO_DISABLE,
	DIO_ENABLE: _binding.B0_DIO_ENABLE,

	misc: { box0v5: { ps: require("./misc/box0-v5/ps.js") } }
}
