/*
 * This file is part of node-box0.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * node-box0 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * node-box0 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with node-box0.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

var ref = require("ref")
var _binding = require("../_binding.js")
var Device = require('../device.js').Device
var act = require('../exception.js').act
var ps = require('../misc/box0-v5/ps.js')

/* usb device */

var UsbDevice = function (pointer) {
	Device.call(this, pointer)
}

UsbDevice.prototype = Object.create(Device.prototype)
UsbDevice.prototype.constructor = UsbDevice

UsbDevice.prototype.libusb_context = function () {
	var ptr = ref.alloc(_binding.ptr_libusb_context)
	act(_binding.b0_usb_libusb_context(this._pointer, ptr))
	return ptr.deref()
}

UsbDevice.prototype.libusb_device = function () {
	var ptr = ref.alloc(_binding.ptr_libusb_device)
	act(_binding.b0_usb_libusb_device(this._pointer, ptr))
	return ptr.deref()
}

UsbDevice.prototype.libusb_device_handle = function () {
	var ptr = ref.alloc(_binding.ptr_libusb_device_handle)
	act(_binding.b0_usb_libusb_device_handle(this._pointer, ptr))
	return ptr.deref()
}

/* box0-v5 */

var Box0V5UsbDevice = function (pointer) {
	UsbDevice.call(this, pointer)
}

Box0V5UsbDevice.prototype = Object.create(UsbDevice.prototype)
Box0V5UsbDevice.prototype.constructor = Box0V5UsbDevice

Box0V5UsbDevice.prototype.PS_PM5 = ps.PM5
Box0V5UsbDevice.prototype.PS_P3V3 = ps.P3V3
Box0V5UsbDevice.prototype.PS_ANALOG = ps.ANALOG
Box0V5UsbDevice.prototype.PS_DIGITAL = ps.DIGITAL

Box0V5UsbDevice.prototype.ps_en_set = ps.en_set
Box0V5UsbDevice.prototype.ps_en_get = ps.en_get
Box0V5UsbDevice.prototype.ps_oc_get = ps.oc_get
Box0V5UsbDevice.prototype.ps_oc_ack = ps.oc_ack

function to_device(pointer) {
	if (_binding.b0v5_valid_test(pointer) == _binding.B0_OK) {
		/* box0-v5 */
		return new Box0V5UsbDevice(pointer)
	}

	return new UsbDevice(pointer)
}

function open_supported() {
	var ret = ref.alloc(_binding.ptr_b0_device)
	act(_binding.b0_usb_open_supported(ret))
	return to_device(ret.deref())
}

exports.open_supported = open_supported
