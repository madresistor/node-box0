var box0 = require("../lib/box0")

var dev = box0.usb.open_supported()
var ain = dev.ain()
ain.snapshot_prepare()

console.log("current bitsize and speed: ", ain.bitsize_speed_get())

ain.close()
dev.close()
