var box0 = require("../lib/box0")

var dev = box0.usb.open_supported()

for (var i in dev.modules) {
	var mod = dev.modules[i]
	console.log("found " + mod.name + " with index " + mod.index)
}

dev.close()
