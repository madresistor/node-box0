var box0 = require("../lib/box0")

var dev = box0.usb.open_supported()
var ain = dev.ain()

var buf = new Float32Array(100)
ain.snapshot_prepare()
ain.snapshot_start(buf)

console.log("got data", buf)

ain.close()
dev.close()
