var box0 = require("../lib/box0")

var dev = box0.usb.open_supported()
var dio = dev.dio()
dio.basic_prepare()
dio.basic_start()

var pin = dio.pin(0)
pin.output()
pin.low()
pin.enable()

setInterval(function () {
	pin.toggle()
}, 500)

process.on('SIGINT', function() {
	console.log("\nGracefully shutting down from SIGINT (Ctrl+C)");
	dio.basic_stop()
	dio.close()
	dev.close()
	process.exit(1)
})
