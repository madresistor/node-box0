var box0 = require("../lib/box0")

var dev = box0.usb.open_supported()
var ain = dev.ain()

console.log({
	buffer_size: ain.buffer_size,
	capab: ain.capab,
	chan_count: ain.chan_count,
	ref: ain.ref,
	label: ain.label,
	snapshot: ain.snapshot,
	stream: ain.stream
})

ain.close()
dev.close()
