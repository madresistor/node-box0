var box0 = require("../lib/box0")

var dev = box0.usb.open_supported()
var ain = dev.ain()

ain.stream_prepare()
ain.bitsize_speed_set(12, 10000)
ain.stream_start()
var buf = new Float32Array(100)

for (var i = 0; i < 100; i++) {
	ain.stream_read(buf)
	console.log(buf)
}

ain.stream_stop()

ain.close()
dev.close()
