"use strict";

var express = require('express')
var box0 = require('../lib/box0')
var util = require('util')

var dev = box0.usb.open_supported()
var dio0 = dev.dio()

var app = express()

function invalid_arg(res, msg) {
	res.set('Content-Type', 'text/plain')
	res.status(400).send(msg)
}

function handle_exception(res, cb) {
	try {
		cb();
	} catch (e) {
		res.set('Content-Type', 'text/plain')
		res.status(500).send(e.message)
	}
}

app.get('/', function (req, res) {
	res.json({
		name: dev.name,
		manuf: dev.manuf,
		serial: dev.serial
	})
})

app.get('/dio/0', function (req, res) {
	var low = dio0.ref.low.toFixed(6)
	var high = dio0.ref.high.toFixed(6)

	res.json({
		name: dio0.header.name,
		count: dio0.pin_count,
		ref: {low, high}
	})
})

app.param('pin', function(req, res, next, pin) {
	var pin_value = parseInt(pin)
	if (pin_value >= 0 && pin_value < dio0.pin_count) {
		req.pin = pin_value
		next()
		return
	}

	invalid_arg(res, util.format('Invalid pin "%s"', pin))
})

app.get('/dio/0/:pin', function (req, res) {
	handle_exception(res, function () {
		var pin = req.pin
		var name = dio0.label.pin[pin]
		if (name == null) {
			name = 'CH' + pin
		}

		var value = dio0.value_get(pin)
		var dir = dio0.dir_get(pin)
		var hiz = dio0.hiz_get(pin)

		value = {true: 'high', false: 'low'}[value]
		dir = {true: 'output', false: 'input'}[dir]
		hiz = {true: 'enabled', false: 'disabled'}[hiz]

		res.json({pin, name, value, dir, hiz})
	})
})

app.get('/dio/0/:pin/value', function (req, res) {
	handle_exception(res, function () {
		var pin = req.pin
		var value = dio0.value_get(pin)
		value = {true: 'high', false: 'low'}[value]
		res.json({pin, value})
	})
})

app.get('/dio/0/:pin/dir', function (req, res) {
	handle_exception(res, function () {
		var pin = req.pin
		var dir = dio0.dir_get(pin)
		dir = {true: 'output', false: 'input'}[dir]
		res.json({pin, dir})
	})
})

app.get('/dio/0/:pin/hiz', function (req, res) {
	handle_exception(res, function () {
		var pin = req.pin
		var hiz = dio0.hiz_get(pin)
		hiz = {true: 'enabled', false: 'disabled'}[hiz]
		res.json({pin, hiz})
	})
})

app.get('/dio/0/:pin/toggle', function (req, res) {
	handle_exception(res, function () {
		var pin = req.pin
		dio0.value_toggle(pin)
		res.json({pin})
	})
})

app.get('/dio/0/:pin/value/:value', function (req, res) {
	var pin = req.pin
	var value = String(req.params.value).toLowerCase()
	value = {'high': true, 'low': false}[value]

	if (value === undefined) {
		var msg = util.format('Invalid value "%s"', req.params.value)
		return invalid_arg(res, msg)
	}

	handle_exception(res, function () {
		dio0.value_set(pin, value)
		res.json({pin})
	})
})

app.get('/dio/0/:pin/dir/:value', function (req, res) {
	var pin = req.pin
	var value = String(req.params.value).toLowerCase()
	value = {'output': true, 'input': false}[value]

	if (value === undefined) {
		var msg = util.format('Invalid value "%s"', req.params.value)
		return invalid_arg(res, msg)
	}

	handle_exception(res, function () {
		dio0.dir_set(pin, value)
		res.json({pin})
	})
})

app.get('/dio/0/:pin/hiz/:value', function (req, res) {
	var pin = req.pin
	var value = String(req.params.value).toLowerCase()
	value = {'enable': true, 'disable': false}[value]

	if (value === undefined) {
		var msg = util.format('Invalid value "%s"', req.params.value)
		return invalid_arg(res, msg)
	}

	handle_exception(res, function () {
		dio0.hiz_set(pin, value)
		res.json({pin})
	})
})

app.listen(3000, function () {
	console.log('Listening on port 3000!')
	dio0.basic_prepare()
	dio0.basic_start()
})

process.on('SIGINT', function() {
	console.log("\nGracefully shutting down from SIGINT (Ctrl+C)")
	dio0.basic_stop()
	dio0.close()
	dev.close()
	process.exit(1)
})
