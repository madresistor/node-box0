var box0 = require('../lib/box0')

var dev = box0.usb.open_supported()

var en = dev.ps_en_get()
var pm5 = !!(en & dev.PS_PM5)
var p3 = !!(en & dev.PS_P3V3)

console.log({'±5V': pm5, '+3.3V': p3})

dev.close()
